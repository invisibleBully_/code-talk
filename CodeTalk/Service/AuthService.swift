//
//  AuthService.swift
//  CodeTalk
//
//  Created by Junior on 04/10/2018.
//  Copyright © 2018 capsella. All rights reserved.
//

import Foundation
import Firebase


class AuthService{
    static let instance = AuthService()
    
    
    func registerUser(withEmail email: String, andPassword password: String, userCreationCompletion: @escaping (_ status: Bool, _ error: Error?) -> ()){
        Auth.auth().createUser(withEmail: email, password: password) { (user, error) in
            guard let user = user else {
                userCreationCompletion(false, error)
                return
            }
            let userData = ["provider": user.user.providerID, "email": user.user.email]
            DataService.instance.createDBUSer(uid: user.user.uid, userData: userData as Dictionary<String, Any>)
            userCreationCompletion(true,nil)
        }
    }
    
    
    
    func loginUser(withEmail email: String, andPassword password: String, loginCompletion: @escaping (_ status: Bool, _ error: Error?) -> ()){
        Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
            guard let user = user else {
                loginCompletion(false,error)
                return
            }
            loginCompletion(true, nil)
            
        }
    }
    
}
