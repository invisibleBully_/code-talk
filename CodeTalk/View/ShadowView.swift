//
//  ShadowView.swift
//  CodeTalk
//
//  Created by Junior on 04/10/2018.
//  Copyright © 2018 capsella. All rights reserved.
//

import UIKit

class ShadowView: UIView {
    
    override func awakeFromNib() {
        self.layer.shadowOpacity = 0.75
        self.layer.shadowRadius = 5
        self.layer.shadowColor =  UIColor.black.cgColor
        super.awakeFromNib()
    }
    
}
